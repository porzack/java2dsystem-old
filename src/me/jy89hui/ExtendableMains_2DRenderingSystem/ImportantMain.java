package me.jy89hui.ExtendableMains_2DRenderingSystem;

import java.text.DateFormat; 
import java.text.SimpleDateFormat;
import java.util.Date;

import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public abstract class ImportantMain {
	public static void main(String[] args){
		log("Program started.");
		logger.FatalError("No main method. please dont use the ImportantMain abstract class without a main method.");
	}
	public static int Times;
	public static void log(String s){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println( "[IM] ["+dateFormat.format(date)+"] ["+Times+"] "+s);
	    
		Times++;
	}
	
}
