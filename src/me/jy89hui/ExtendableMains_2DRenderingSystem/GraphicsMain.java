package me.jy89hui.ExtendableMains_2DRenderingSystem;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import me.jy89hui.AMain.Main2D;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartLog;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;
import me.jy89hui.Objects_2DRenderingSystem.ConsoleLogger;
import me.jy89hui.Objects_2DRenderingSystem.Menu;

public abstract class GraphicsMain extends JPanel {
	public static String version = "2.5";
	public static ConsoleLogger Console = null;
	public static JFrame frame = new JFrame();
	public static JPanel panel;
	public static Menu menu = null;
	public static float avgPrintTime = 0;
	public static int canBePainted = 0;
	public static int refreshRate = 20;
	public static Timer timer;
	public static ArrayList<SmartLog> LogRecord = new ArrayList<SmartLog>();
	public static ArrayList<SmartComponent> RealComponents = new ArrayList<SmartComponent>();
	private static HashMap<Integer, Long> timeMeasurements = new HashMap<Integer, Long>();

	public void Start() {
		log("Program started.");
		logger.FatalError("No main method. please dont use the GraphicsMain abstract class without a Start() method.");
	}


	public void RefreshTimerDelay() {
		this.timer.setDelay(this.refreshRate);
	}
	public static void LoadFrame(int locx, int locy, int sizex, int sizey,
			Container c, GraphicsMain gm) {
		logger.loadLogger(gm);
		frame.setLocation(locx, locy);
		frame.setSize(sizex, sizey);
		timer = new Timer(refreshRate, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GraphicsMain.canBePainted++;
				getFrame().repaint();
			}
		});
		timer.setRepeats(true);
		timer.setCoalesce(true);
		timer.start();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				log("Exit button pressed. Shutting down.");
				log(" [IMPORTANT] This is a personal reminder to add saving methods in the LoadFrame method (GraphicsMain class). at: addWindowListener");
				System.exit(0);
			}
		});
		frame.repaint();
		panel = (JPanel) c;
		frame.setContentPane(panel);
		frame.setVisible(true);
		log("Frame Created");
	}

	public static JFrame getFrame() {
		return frame;
	}

	public static JPanel getPanel() {
		return panel;
	}

	public void setToHighestPriority(SmartComponent s) { // So that the frames
															// overlap eachother
															// correctly
		this.RealComponents.remove(s);
		this.RealComponents.add(s); // This will make it so the component is
									// painted last (giving it the ontop of
									// feeling)
		this.panel.remove(s);
		this.panel.add(s);
	}

	public static int Times;

	public static void log(String s) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("[GM] [" + dateFormat.format(date) + "] [" + Times
				+ "] " + s);
		Times++;
	}

	public static void beginTimeMeasurement(int ID) {
		timeMeasurements.put(ID, System.nanoTime());
	}

	public static float endTimeMeasurement(int ID, boolean log) {
		try {
			long startTime = timeMeasurements.get(ID);
			long currentTime = System.nanoTime();
			long diff = currentTime - startTime;
			timeMeasurements.remove(ID);
			if (log) {
				logger.silentlog("[TimeMeasurement] ID:" + ID + ", StartTime:"
						+ startTime + ", EndTime:" + currentTime + ", diff:"
						+ diff + " TimeInMilliseconds:" + diff / 1000000);
			}
			return (float) diff / 1000000;
		} catch (Exception e) {
			// It does not matter
			return 6969;
		}
	}

}
