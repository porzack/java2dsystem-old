package me.jy89hui.Interfaces_2DRenderingSystem;

public interface MovableComponent {
	public void TranslateLocation(int dx, int dy); // dx = Distance of translation x (Increment x) dy = Distance of translation y (Increment y)
	public void TranslateSize(int sx, int sy); // sx = Increment x, sy= Increment y (CAN BE NEGATIVE VALUES)
}
