package me.jy89hui.Interfaces_2DRenderingSystem;

//import java.awt.Color;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Random;

import javax.swing.JTextArea;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class SmartTextArea extends JTextArea {
	public enum typeOfTextArea {
		Logger, PlainText, TextInput
	}

	public typeOfTextArea textAreaType;
	private int x, y, sx, sy;
	private String newLineSymbol = "∫";
	private GraphicsMain graphicsMain;
	private boolean randomFunColors = false;
	/**
	 * We do an arraylist here so that if you want logging style type data you
	 * can. If you dont want logging style type data you can just use one log
	 * for all your text
	 */
	/** 
	 * 
	 * SL STANDS FOR SMART LOG THROUGHOUT THE ENTIRE PROGRAM
	 * 
	 */
	
	
	
	/*
	 * |-------------------|
	 * |  12.5             |
	 * |            72     |
	 * |                   |
	 * |                   |
	 * |      ENTRANCE     |
	 * |         TO        |
	 * |       MAGIC       |
	 * |      NUMBER       |
	 * |       HELL!       |             <------- ENTER HERE 
	 * |                   |
	 * |   4.26            |
	 * |                   |
	 * |       3           |
	 * |              22   |
	 * |                   |
	 * |     12.111        |
	 * |                   |
	 * |                   |
	 * |-------------------|
	 * 
	 * 
	 */
	private boolean showingBackground = true;
	private boolean lineScrolling = false;
	private ArrayList<SmartLog> text = new ArrayList<SmartLog>();

	public SmartTextArea(int x, int y, int sx, int sy, GraphicsMain gm) {
		this.graphicsMain=gm;
		this.x = x;
		this.y = y;
		this.sx = sx;
		this.sy = sy;
		this.setBounds(x, y, sx, sy);
		this.graphicsMain.getFrame().add(this);
		// this.addMouseListener(this);
	}

	public int getSizeY() {
		return this.sy;
	}

	public void setLineScrolling(boolean option) {
		this.lineScrolling = option;
	}

	public boolean getLineScrolling() {
		return this.lineScrolling;
	}

	public void setShowingBackground(boolean showing) {
		this.showingBackground = showing;
	}

	public void setTextSL(String s) { // must me SL or we cant use .settext
		String[] messages = s.split(this.newLineSymbol);
		ArrayList<SmartLog> sl = new ArrayList<SmartLog>(); // sl stands for
															// smartlog
		for (int x = 0; x < messages.length; x++) {
			sl.add(new SmartLog(messages[x]));
		}
		this.text = sl;
	}

	public void setTextSL(String s, Color c) {
		String[] messages = s.split(this.newLineSymbol);
		ArrayList<SmartLog> sl = new ArrayList<SmartLog>(); // sl stands for
															// smartlog
		for (int x = 0; x < messages.length; x++) {
			sl.add(new SmartLog(messages[x], c));
		}
		this.text = sl;
	}

	public void setTextSL(ArrayList<SmartLog> sl) {
		this.text = sl;
	}

	public String getTextSL() {
		String total = "";
		for (int x = 0; x < this.text.size(); x++) {
			total = total + this.text.get(x).getMessage();
		}
		return total;
	}

	public boolean containsEscapeCharacter(String s) {
		for (int i = 0; i < s.length(); i++)
			switch (s.charAt(i)) {
			case '\n':
				return true;
			case '\t':
				return true;
			}
		return false;
	}

	public void PrintComp(Graphics g) {
		/**
		 * Draw the outline of the shape //
		 * 
		 * */
		if (this.showingBackground) {
			g.setColor(new Color(236, 237, 230));
			g.fillRoundRect(this.x, this.y, this.sx, this.sy, this.sx / 7,
					this.sy / 10);
			g.setColor(new Color(69, 69, 71));
			g.drawRoundRect(this.x, this.y, this.sx, this.sy, this.sx / 7,
					this.sy / 10);
		}
		g.setColor(new Color(0, 0, 0));
		String[] splitText = this.getText().split(this.newLineSymbol);
		/**
		 * Begin to draw the strings and text
		 */
		if (this.textAreaType == typeOfTextArea.TextInput) { // If we want the
																// text cented
																// on the Y
			// System.out.println(this.unEscapeString("Txt:" +this.getText()));
			ArrayList<SmartLog> test = new ArrayList<SmartLog>();
			test.add(new SmartLog(this.getText()));
			this.text = test;
			if (this.containsEscapeCharacter(this.getText())) {
				this.ClearText();
			}
			g.drawString(this.getText(), this.x + 10, this.y + (this.sy / 2)
					+ 6); // + 6 because the box size*/

		} else if (this.textAreaType == typeOfTextArea.Logger
				|| this.textAreaType == typeOfTextArea.PlainText) {
			if (!this.randomFunColors) {
				int currentDistortionY = 0;// Distortion on Y axis
				for (int x = 0; x < splitText.length; x++) {

					int lengthOfMessageSoFar = 0; // How many characters of the
													// main
					// message have been printed
					if (this.lineScrolling) {
						this.doLineScrollCalculations();
					}
					/**
					 * Explanation of what is about to happen: Text can have
					 * many different strings, cycle through and draw them Each
					 * string may have individual strings that also hold data
					 * and we then draw those
					 */
					int numberOfStrings = this.text.size(); // we just assume
															// text
					// is right
					for (int number = 0; number < numberOfStrings; number++) {
						for (int message = 0; message < this.text.get(number)
								.getMessages().size(); message++) {
							g.setColor(this.text.get(number).getColors()
									.get(message)); // set it to the correct
													// color
							g.setFont(this.text.get(number).getFonts()
									.get(message)); // set it to the correct
													// color
							g.drawString(
									this.text.get(number).getMessages()
											.get(message),
									(int) (this.x
											+ (lengthOfMessageSoFar * 6.3) + 10),
									(int) (this.y + (currentDistortionY)
											+ (((float) this.sy / 100) * 6) + 10));
							lengthOfMessageSoFar = lengthOfMessageSoFar
									+ this.text.get(number).getMessages()
											.get(message).length();
						}

						lengthOfMessageSoFar = 0;
						currentDistortionY = currentDistortionY
								+ g.getFont().getSize();
					}
				}
			} else {
				int currentDistortionY = 0;// Distortion on Y axis
				Random rand = new Random();
				for (int x = 0; x < splitText.length; x++) {

					int lengthOfMessageSoFar = 0; // How many characters of the
													// main
					// message have been printed
					if (this.lineScrolling) {
						this.doLineScrollCalculations();
					}
					int numberOfStrings = this.text.size(); // we just assume
															// text
					// is right
					
					for (int number = 0; number < numberOfStrings; number++) {
						for (int message = 0; message < this.text.get(number)
								.getMessage().split(" ").length; message++) {
							g.setColor(new Color(rand.nextInt(30), rand
									.nextInt(255), rand.nextInt(255)));
							g.drawString(
									this.text.get(number).getMessage()
											.split(" ")[message]
											+ " ",
									(int) (this.x
											+ (lengthOfMessageSoFar * 6.3) + 10),
									(int) (this.y + (currentDistortionY * 16)
											+ (((float) this.sy / 100) * 6) + 10));
							lengthOfMessageSoFar = lengthOfMessageSoFar
									+ this.text.get(number).getMessage()
											.split(" ")[message].length() + 1;
						}

						lengthOfMessageSoFar = 0;
						currentDistortionY++;
					}
				}
			}

		}

	}

	public void doLineWrapCalculations() {
		//TODO this
	}

	public void doLineScrollCalculations() {
		/**
		 * \/ How many PX a line is
		 */
		// logger.log("Sze: "+this.text.size(), new Color(255,0,20));
		try {
			boolean notOk = true;
			while (notOk) {
				int totalTextSize = 0;

				for (SmartLog l : this.text) {
					totalTextSize = totalTextSize
							+ l.getFonts().get(0).getSize();
				}
				if ((totalTextSize) > (this.sy * 0.90)) {
					this.text.remove(0); // Remove first one in the array
				} else {
					notOk = false;
				}
			}
		} catch (ConcurrentModificationException e) {
			logger.normalError("Concurrent Modification Exception. At SmartTextArea > doLineScrollCalculations");
			//e.printStackTrace();
		}

	}

	public void Update(int x, int y, int sx, int sy) {
		this.setBounds(x, y, sx, sy);
		this.x = x;
		this.y = y;
		this.sx = sx;
		this.sy = sy;
	}

	public void ClearText() {
		this.setCaretPosition(0);
		this.setText("");
	}

	@Override
	public void paint(Graphics g) {
		// logger.log("JButton Paint called");

	}

	@Override
	public void print(Graphics g) {
		logger.log("JTextArea print called");
	}

}
