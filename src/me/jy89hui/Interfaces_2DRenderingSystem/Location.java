package me.jy89hui.Interfaces_2DRenderingSystem;

public class Location {
	public double x;
	public double y;
	public Location(double x, double y){
		this.x=x;
		this.y=y;
	}
	public String toString(){
		return "Location: ["+this.x+","+this.y+"]";
	}
	public Location add(double x, double y){
		this.x=this.x+x;
		this.y=this.y+y;
		return this;
	}
	public Location add(Location loc){
		this.x=this.x+loc.x;
		this.y=this.y+loc.y;
		return this;
	}
	public Location subtract(Location loc){
		this.x=this.x-loc.x;
		this.y=this.y-loc.y;
		return this;
	}
	public Location subtract(double x, double y){
		this.x=this.x-x;
		this.y=this.y-y;
		return this;
	}
	public Location multiply(double amount){
		this.x=this.x*amount;
		this.y=this.y*amount;
		return this;
	}
	public Location devide(double amount){
		this.x=this.x/amount;
		this.y=this.y/amount;
		return this;
	}
	public boolean equals(Location other){
		if (this.x== other.x && this.y==other.y){
			return true;
		}
		return false;
	}
}
