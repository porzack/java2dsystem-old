package me.jy89hui.Interfaces_2DRenderingSystem;

import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class Command {
	private String[] args;
	private String Cmd;
	private String FullCommand;
	private boolean handled = false;

	public Command(String Cmd, String[] args, String FullCommand) {
		this.args = args;
		this.Cmd = Cmd;
		this.setFullCommand(FullCommand);
	}

	public boolean isHandled() {
		return this.handled;
	}

	public void Handled() {
		this.handled = true;
		logger.log("Command: [ " + this.getFullCommand()
				+ " ] has been handled with success.");
	}
	public void Handled(boolean logIt){
		this.handled=true;
		if (logIt){
			logger.log("Command: [ " + this.getFullCommand()
					+ " ] has been handled with success.");
		}
	}

	public String[] getArgs() {
		return this.args;
	}

	public void setArgs(String[] args) {
		this.args = args;
	}

	public String getCmd() {
		return this.Cmd;
	}

	public void setCmd(String cmd) {
		this.Cmd = cmd;
	}

	public String getFullCommand() {
		return this.FullCommand;
	}

	public void setFullCommand(String fullCommand) {
		this.FullCommand = fullCommand;
	}

}
