package me.jy89hui.Interfaces_2DRenderingSystem;

import java.awt.Component;
import java.awt.Graphics;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public abstract class SmartComponent extends Component {
	protected int x = 0, y = 0, sx = 50, sy = 50;
	private GraphicsMain mainclass;
	private String name = "Null";
	private boolean mouseInsideSmartComp=false;

	public int getLocX() {
		try {
			return this.x;
		} catch (Exception e) {
			return 420;
		}
	}

	public int getLocY() {
		return this.y;
	}

	public int getSizeX() {
		return this.sx;
	}

	public int getSizeY() {
		return this.sy;
	}

	public void setGraphicsMain(GraphicsMain gm) {
		this.mainclass = gm;
	}

	public void onCommand(Command c) {
		if (c.getArgs()[1].equalsIgnoreCase("SetSize")) {
			try {
				this.sx = Integer.parseInt(c.getArgs()[2]);
				this.sy = Integer.parseInt(c.getArgs()[3]);
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName()
						+ " Setsize <SizeXInt> <SizeYInt>");
			}
		} else if (c.getArgs()[1].equalsIgnoreCase("SetLocation")) {
			try {
				this.x = Integer.parseInt(c.getArgs()[2]);
				this.y = Integer.parseInt(c.getArgs()[3]);
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName()
						+ " SetLocation <LocXInt> <LocYInt>");
			}
		} else if (c.getArgs()[1].equalsIgnoreCase("setname")) {
			try {
				this.setName(c.getArgs()[2]);
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName() + " Setname <NameString>");
			}
		} else if (c.getArgs()[1].equalsIgnoreCase("SomeMenuCommand")) {
			try {
				logger.log("PROGRESS");
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName() + " Something... ");
			}
		} else {

		}
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String n) {
		this.name = n;
	}
	/**
	 * This is a simple method that is very important, we force all the extenders to be painted onto the frame.
	 * @param g
	 */
	public abstract void PrintComp(Graphics g);

	public void moveToTopPriority() {
		this.mainclass.setToHighestPriority(this);
		this.mainclass.getPanel().remove(this);
		this.mainclass.getPanel().add(this);
	}

	public boolean toggle(boolean b) {
		if (b) {
			return false;
		}
		if (!b) {
			return true;
		}
		logger.FatalError("Graphics manager cannot figure out how to toggle a boolean.");
		return false;
	}
}
