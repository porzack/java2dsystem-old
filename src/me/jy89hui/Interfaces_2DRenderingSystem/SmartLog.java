package me.jy89hui.Interfaces_2DRenderingSystem;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class SmartLog {
	private ArrayList<String> messages = new ArrayList<String>(); // Holds all
																	// messages
																	// \/
	private ArrayList<Font> fonts = new ArrayList<Font>();
	private Font defaultFont = new Font("Times New Roman", Font.PLAIN, 12);
	private ArrayList<Color> colors = new ArrayList<Color>(); // Hold all the
																// colors of the
																// strings so
																// that I can
																// alternate
																// colors.
	private Color defaultColor = new Color(0, 0, 0);

	public SmartLog(String msg) {
		//Random rand = new Random();this.defaultColor  = new Color(10, rand.nextInt(255),rand.nextInt(255));
		this.messages.add(msg);
		this.colors.add(this.defaultColor);
		this.fonts.add(this.defaultFont);
	}
	public SmartLog(String msg, Font f) {
		//Random rand = new Random();this.defaultColor  = new Color(10, rand.nextInt(255),rand.nextInt(255));
		this.messages.add(msg);
		this.colors.add(this.defaultColor);
		this.fonts.add(f);
	}

	public SmartLog(String msg, Color c) {
		//Random rand = new Random();c  = new Color(10, rand.nextInt(255),rand.nextInt(255));
		this.messages.add(msg);
		this.colors.add(c);
		this.fonts.add(this.defaultFont);
	}
	public SmartLog(String msg, Color c, Font f) {
		//Random rand = new Random();c  = new Color(10, rand.nextInt(255),rand.nextInt(255));
		this.messages.add(msg);
		this.colors.add(c);
		this.fonts.add(f);
	}

	public String getMessage() {
		String message = "";
		for (String s : this.messages) { // loop through all messages and add
											// them
			message = message + s;
		}
		return message;
	}

	public void addText(String s) { // Add a string of text to the smart comp
		this.messages.add(s);
		this.colors.add(this.defaultColor);
		this.fonts.add(this.defaultFont);
	}

	public void addColoredText(String s, Color c) { // add a string of color
		this.messages.add(s);
		this.colors.add(c);
		this.fonts.add(this.defaultFont);
	}
	public void addFont(String s, Font f){
		this.messages.add(s);
		this.colors.add(this.defaultColor);
		this.fonts.add(f);
	}

	public ArrayList<Color> getColors() {
		return this.colors;
	}

	public ArrayList<String> getMessages() {
		return this.messages;
	}
	public ArrayList<Font> getFonts() {
		return this.fonts;
	}
	public int getNumberOfMessages() {
		if (this.messages.size() == this.colors.size()&& this.messages.size() == this.fonts.size()) {
			return this.messages.size();
		} else {
			logger.FatalError("The SmartLog with text: " + this.getMessage()
					+ " does not have the same amount of messages, colors and fonts.");
			return 0;
		}

	}
}
