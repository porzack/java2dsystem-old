package me.jy89hui.Interfaces_2DRenderingSystem;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class SmartButton extends SmartComponent implements MouseListener {
	public enum typeOfButton {
		Normal, Options, TrueFalse
	}

	private typeOfButton buttonType = typeOfButton.Normal;
	private SmartLog text;
	private Color Color = new Color(240, 252, 252);
	private Action action;
	private boolean actionSet = false;
	private GraphicsMain graphicsMain;
	private int x,y,sx,sy;
	private ArrayList<SmartLog> Options = new ArrayList<SmartLog>();// Used for
																	// Options
																	// Mode
	private int selectedOption = 0;
	private boolean currentValue = false; // For trueFalse type of button

	public SmartButton(int x, int y, int sx, int sy, GraphicsMain gm) {
		this.graphicsMain=gm;
		this.x = x;
		this.y = y;
		this.sx = sx;
		this.sy = sy;
		this.text = new SmartLog("");
		this.setBounds(this.x, this.y, this.sx, this.sy);
		addMouseListener(this);
		this.graphicsMain.getPanel().add(this);
	}

	public SmartButton(int x, int y, int sx, int sy, String s, GraphicsMain gm) {
		this.graphicsMain=gm;
		this.x = x;
		this.y = y;
		this.sx = sx;
		this.sy = sy;
		this.text = new SmartLog(s);
		this.setBounds(this.x, this.y, this.sx, this.sy);
		addMouseListener(this);
		this.graphicsMain.getPanel().add(this);
	}

	public SmartButton(int x, int y, int sx, int sy, SmartLog s, GraphicsMain gm) {
		this.graphicsMain=gm;
		this.x = x;
		this.y = y;
		this.sx = sx;
		this.sy = sy;
		this.text = s;
		this.setBounds(this.x, this.y, this.sx, this.sy);
		addMouseListener(this);
		this.graphicsMain.getPanel().add(this);
	}

	public void setOptions(ArrayList<SmartLog> options) {
		this.Options = options;
	}

	public SmartLog getSelectedOption() {
		if (this.buttonType == typeOfButton.Options) {
			return this.Options.get(this.selectedOption);
		} else {
			logger.normalError("You have requested a option value from the "
					+ this.text.getMessage()
					+ " button. (Its not a option button)");
			return null;
		}
	}

	public boolean getTrueFalseValue() {
		if (this.buttonType == typeOfButton.TrueFalse) {
			return this.currentValue;
		} else {
			logger.normalError("You have requested a true false value from the "
					+ this.text.getMessage()
					+ " button. (Its not a t/f button)");
			return false;
		}
	}

	public void setButtonType(typeOfButton t) {
		this.buttonType = t;
		if (t == typeOfButton.Normal) {
			logger.silentlog("Button " + this.text.getMessage()
					+ " is set to Normal");
		}
		if (t == typeOfButton.Options) {
			logger.silentlog("Button " + this.text.getMessage()
					+ " is set to Options");
			this.actionSet = true;
			this.action = new Action() {
				@Override
				public void go() {
					selectedOption++;
					if (selectedOption >= Options.size()) {
						selectedOption = 0;
					}
					// logger.log("Selected Option is "+selectedOption);
				}
			};

		}
		if (t == typeOfButton.TrueFalse) {
			logger.silentlog("Button " + this.text.getMessage()
					+ " is set to True/False");
			this.actionSet = true;
		}
	}

	public void setText(String s) {
		this.text = new SmartLog(s);
	}

	public void setText(SmartLog s) {
		this.text = s;
	}

	public String getText() {
		return this.text.getMessage();
	}

	public SmartLog getExactText() {
		return this.text;
	}

	@Override
	public void PrintComp(Graphics g) {
		if (this.text.getColors().size() > 1) {
			logger.normalError("Cannot draw button with text: "
					+ this.text.getMessage());
			logger.normalError("You have provided too many colors, the maximum is 1 color.");
			logger.warning("Object not drawn.");
			return;
		} else {
			if (this.buttonType == typeOfButton.Normal) {
				g.setColor(this.Color);
				g.fillRoundRect(this.x, this.y, this.sx, this.sy, this.sx / 5,
						this.sy / 5);
				g.setColor(this.text.getColors().get(0));
				g.setFont(this.text.getFonts().get(0));
				g.drawRoundRect(this.x, this.y, this.sx, this.sy, this.sx / 5,
						this.sy / 5);
				g.drawString(this.getExactText().getMessage(), this.x
						+ (this.sx / 2)
						- this.getExactText().getMessage().length()
						* g.getFont().getSize() / 4, this.y + (this.sy / 2) + 5);
			}
			if (this.buttonType == typeOfButton.Options) {
				g.setColor(this.Color);
				g.fillRoundRect(this.x, this.y, this.sx, this.sy, this.sx / 5,
						this.sy / 5);
				g.setColor(this.Options.get(this.selectedOption).getColors()
						.get(0));
				g.setFont(this.Options.get(this.selectedOption).getFonts()
						.get(0));
				g.drawRoundRect(this.x, this.y, this.sx, this.sy, this.sx / 5,
						this.sy / 5);
				g.drawString(
						this.Options.get(this.selectedOption).getMessage(),
						this.x
								+ (this.sx / 2)
								- this.Options.get(this.selectedOption)
										.getMessage().length() * 3, this.y
								+ (this.sy / 2) + 5);
			}
			if (this.buttonType == typeOfButton.TrueFalse) {
				g.setColor(this.Color);
				g.fillRoundRect(this.x, this.y, this.sx, this.sy, this.sx / 5,
						this.sy / 5);
				if (this.currentValue == true) {
					g.setColor(new Color(20, 255, 20));
					g.drawRoundRect(this.x, this.y, this.sx, this.sy,
							this.sx / 5, this.sy / 5);
					g.drawString("True",
							this.x + (this.sx / 2) - "True".length() * 3,
							this.y + (this.sy / 2) + 5);
				} else {
					g.setColor(new Color(255, 20, 20));
					g.drawRoundRect(this.x, this.y, this.sx, this.sy,
							this.sx / 5, this.sy / 5);
					g.drawString("False",
							this.x + (this.sx / 2) - "False".length() * 3,
							this.y + (this.sy / 2) + 5);
				}

			}

		}
	}

	public void setColor(Color c) {
		this.Color = c;
	}

	public void Update(int x, int y, int sx, int sy) {
		this.x = x;
		this.y = y;
		this.sx = sx;
		this.sy = sy;
		this.setBounds(this.x, this.y, this.sx, this.sy);
	}

	@Override
	public void print(Graphics g) {
		logger.normalError("Component print was called on a button, Invalid.");
	}

	public void enableAction(Action a) {
		this.actionSet = true;
		this.action = a;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		if (this.actionSet) {
			this.action.go();
		}
		if (this.buttonType== typeOfButton.TrueFalse){
			if (currentValue == true) {
				currentValue = false;
			} else {
				currentValue = true;
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// logger.log("Entered", new Color(0,255,0));
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// logger.log("Exited", new Color(255,0,0));
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// logger.log("Pressed", new Color(0,255,0));
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// logger.log("Released", new Color(0,255,0));

	}
}
