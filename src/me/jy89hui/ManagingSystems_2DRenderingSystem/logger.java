package me.jy89hui.ManagingSystems_2DRenderingSystem;

import java.awt.Color;
import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartLog;

public class logger {
	private static GraphicsMain mainclass;
	public static void loadLogger(GraphicsMain gm){
		mainclass=gm;
	}
	private static void Elog(String s) { // Stands for Error Log
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("[[[[ Error ]]]] [" + dateFormat.format(date) + "] "
				+ s);
		mainclass.LogRecord.add(new SmartLog("[[[[ Error ]]]] " + s,
				new Color(255, 72, 0)));
		if (mainclass.Console != null) {
			mainclass.Console.UpdateText();
		}
	}

	private static void Wlog(String s) { // Stands for Warning Log
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("[[[[ Warning ]]]] [" + dateFormat.format(date)
				+ "] " + s);
		mainclass.LogRecord.add(new SmartLog(
				"[[[[ Warning ]]]] " +s, new Color(230, 100, 42)));
		if (mainclass.Console != null) {
			mainclass.Console.UpdateText();
		}
	}

	private static void EXIT(String s) {
		Elog("--------------------------------------------------------------------------");
		Elog("A FATAL ERROR HAS OCCURED! ");
		Elog("ERROR: " + s);
		Elog("Program stopped.");
		System.exit(0);
	}

	private static void EXIT() {
		Elog("--------------------------------------------------------------------------");
		Elog("A FATAL ERROR HAS OCCURED! ");
		Elog("Program stopped.");
		System.exit(0);
	}

	public static void FatalError() {
		EXIT();
	}

	public static void FatalError(String s) {
		EXIT(s);
	}

	public static void normalError() {
		Elog("An error has occured.");
	}

	public static void normalError(String s) {
		Elog(s);
	}

	public static void warning(String s) {
		Wlog(s);
	}

	public static void log(String s) { // send a message
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("[log] [" + dateFormat.format(date) + "] " + s);
		mainclass.LogRecord.add(new SmartLog("[log] " + s));
		if (mainclass.Console != null) {
			mainclass.Console.UpdateText();
		}
	}

	public static void log(String s, Color c) { // send a colored message
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("[log] [" + dateFormat.format(date) + "] " + s);
		mainclass.LogRecord.add(new SmartLog("[log] " + s, c));
		if (mainclass.Console != null) {
			mainclass.Console.UpdateText();
		}
	}
	public static void log(String s, Color c, Font f) { 
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("[log] [" + dateFormat.format(date) + "] " + s);
		mainclass.LogRecord.add(new SmartLog("[log] " + s, c,f));
		if (mainclass.Console != null) {
			mainclass.Console.UpdateText();
		}
	}
	public static void logLastLine(String s, Color c) { // Add different colored
														// text to the last line
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("[logAddToLastLine] [" + dateFormat.format(date) + "] " + s);
		mainclass.LogRecord.get(mainclass.LogRecord.size()-1).addColoredText(s, c);
		if (mainclass.Console != null) {
			mainclass.Console.UpdateText();
		}
	}

	public static void silentlog(String s) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println("[log] [" + dateFormat.format(date) + "] " + s);
	}
}
