package me.jy89hui.ManagingSystems_2DRenderingSystem;

import me.jy89hui.AMain.Main2D;
import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Command;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartLog;

public class CommandManager {
	

	public void Manage(Command c, GraphicsMain gm) {
		BasicManage(c,gm);
	}

	public void BasicManage(Command c, GraphicsMain gm) {
		if (c.getCmd().equalsIgnoreCase("Stop")) {
			logger.log("Stopping program");
			logger.log(" [IMPORTANT] Add saving methods to CommandManager. at: Manage>Stop");
			c.Handled();// Because why not XD
			System.exit(0);
		} else if (c.getCmd().equalsIgnoreCase("setRefreshRate")) {
			if (Integer.parseInt(c.getArgs()[1]) >= 15) {
				gm.refreshRate = Integer
						.parseInt(c.getArgs()[1]);
				gm.RefreshTimerDelay();
				logger.log("Current MAX FPS is set to: "
						+ (1000 / Integer.parseInt(c.getArgs()[1])));
			} else {
				logger.log("You cannot set a refresh rate lower than 15.");
			}
			c.Handled(false);
		} else if (c.getCmd().equalsIgnoreCase("getRefreshRate")) {
			logger.log("Refresh Rate: " + gm.refreshRate);
			c.Handled(false);
		} else if (c.getCmd().equalsIgnoreCase("ClearLog")) {
			for (int x = 0; x < 100; x++) {
				System.out.println(" ");
				Main2D.LogRecord.add(new SmartLog(" "));
				if (!gm.Console.equals(null)) {
					gm.Console.UpdateText();
				}
			}
			logger.log("Log cleared");
			c.Handled();
		} else if (c.getCmd().equalsIgnoreCase("version")) {
			logger.log("Version: "+GraphicsMain.version);
			c.Handled(false);
		}

	}
}
