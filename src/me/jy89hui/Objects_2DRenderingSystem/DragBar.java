package me.jy89hui.Objects_2DRenderingSystem;

import java.awt.Color;  
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.event.MouseInputListener;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Command;
import me.jy89hui.Interfaces_2DRenderingSystem.MovableComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;
 
public class DragBar extends SmartComponent implements MouseInputListener{
private int sizex,sizey,MouseLocationBeforeChangeX,MouseLocationBeforeChangeY;
private boolean IsMoving=false;
private MovableComponent mainComponent; // MainComponent = What the DragBar is attached to
private SmartComponent smartmainComponent;
private GraphicsMain gm;
public DragBar(int x, int y, int sizex, int sizey, SmartComponent mc, GraphicsMain gm){
	this.x=x;
	this.y=y-20;
	this.sizex=sizex;
	this.sizey=20;
	this.gm=gm;
	this.mainComponent=(MovableComponent) mc;
	this.smartmainComponent=mc;
	this.setBounds((int)this.x, (int)this.y, this.sizex, this.sizey);
	addMouseListener(this);
	addMouseMotionListener(this);
}
public void Update(int x, int y, int sizex, int sizey){
	this.x=x;
	this.y=y-20;
	this.sizex=sizex;
	this.sizey=20;
	this.setBounds((int)this.x, (int)this.y, this.sizex, this.sizey);
}

public void setX(int x) {
	this.x = x;
}


public void setY(int y) {
	this.y = y;
}

public int getSizex() {
	return this.sizex;
}

public void setSizex(int sizex) {
	this.sizex = sizex;
}

public int getSizey() {
	return this.sizey;
}

public void setSizey(int sizey) {
	this.sizey = sizey;
}
public boolean isMoving() {
	return this.IsMoving;
}
public void setIsMoving(boolean isMoving) {
	this.IsMoving = isMoving;
}

@Override
public void mouseClicked(MouseEvent arg0) {
	//logger.log("Mouse clicked.");
	
}

@Override
public void mouseEntered(MouseEvent arg0) {
}

@Override
public void mouseExited(MouseEvent arg0) {	
}

@Override
public void mousePressed(MouseEvent arg0) {
	this.gm.setToHighestPriority(this.smartmainComponent);
	//this.moveToTopPriority();
	this.MouseLocationBeforeChangeX=arg0.getX();
	this.MouseLocationBeforeChangeY= arg0.getY();
	//Error.log("Mouse pressed, Data: "+this.MouseLocationBeforeChangeX+", "+this.MouseLocationBeforeChangeY);
	this.IsMoving=true;
	// ADD MULTI THREADED LOOP 
}	

@Override
public void mouseReleased(MouseEvent arg0) {
	
	this.IsMoving=false;
	int Distancex,Distancey;
	Distancex=this.MouseLocationBeforeChangeX-arg0.getX();
	Distancey=this.MouseLocationBeforeChangeY-arg0.getY();
	this.x=this.x-Distancex;
	this.y=this.y-Distancey;
	this.setBounds((int)this.x, (int)this.y, this.sizex, this.sizey);
	try{
	this.mainComponent.TranslateLocation(Distancex, Distancey);
	} catch(NullPointerException e){logger.normalError("NPE has occured at Class: Dragbar, Method: MouseReleased ");}
	
}

@Override
public void mouseDragged(MouseEvent arg0) {
	if(this.IsMoving){
		int Distancex,Distancey;
	Distancex=this.MouseLocationBeforeChangeX-arg0.getX();
	Distancey=this.MouseLocationBeforeChangeY-arg0.getY();
	this.x=this.x-Distancex;
	this.y=this.y-Distancey;
	this.setBounds((int)this.x, (int)this.y, this.sizex, this.sizey);
	try{
	this.mainComponent.TranslateLocation(Distancex, Distancey);
	} catch(NullPointerException e){logger.normalError("NPE has occured at Class: Dragbar, Method: MouseReleased ");}
	}
}

@Override
public void mouseMoved(MouseEvent arg0) {
	//Error.log("Mouse moved");
	
	
	
}

@Override
public void PrintComp(Graphics g) {
	Graphics2D g2= (Graphics2D) g;
	g2.setColor(new Color(51,45,47));
	//g2.setColor(Color.BLACK);
	//g.fillRect(this.x, this.y, this.sizex, this.sizey);
	g2.fillRoundRect((int)this.x, (int)this.y, this.sizex, this.sizey, 10,10);
	g2.setColor(new Color(31,22,3));
	g2.drawRoundRect((int)this.x, (int)this.y, this.sizex, this.sizey, 10,10);
	g2.setColor(new Color(69,124,214));
	g2.drawString(this.smartmainComponent.getName(), (int)this.x+(this.sizex/2)-(this.smartmainComponent.getName().length()*4), (int)this.y+(this.sizey/2)+3);
	g2.setColor(Color.gray);
	
}

@Override
public void onCommand(Command c) {
}

}
