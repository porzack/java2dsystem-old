package me.jy89hui.Objects_2DRenderingSystem;

import java.awt.Color;  
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.event.MouseInputListener;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Command;
import me.jy89hui.Interfaces_2DRenderingSystem.MovableComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;

public class ComponentScaler extends SmartComponent implements MouseInputListener {
	private int x,y,sx,sy;
	private int mouseXbeforeTrans;
	private int mouseYbeforeTrans;
	private int MinX=100;
	private int MinY=100;
	private int size = 30;
	private boolean IsMoving=false;
	private SmartComponent mainCompSmart;
	private MovableComponent mainCompMovable;
	private GraphicsMain gm;
	public ComponentScaler(int x, int y, int sx, int sy, SmartComponent sc, GraphicsMain gm){
		this.x=x;
		this.y=y;
		this.sx=sx;
		this.sy=sy;
		this.mainCompSmart = sc;
		this.mainCompMovable= (MovableComponent) this.mainCompSmart;
		this.gm=gm;
		// Now lets put it in the corner
		this.x=this.x+this.sx-this.size;
		this.y=this.y+this.sy-this.size;
		this.sx=30;
		this.sy=30;
		this.setBounds(this.x, this.y, this.sx, this.sy);
		addMouseListener(this);
		addMouseMotionListener(this);
		gm.getPanel().add(this);
		gm.getFrame().add(this);
	}
	public void Update(int x, int y, int sizex, int sizey){
		this.x=x;
		this.y=y;
		this.sx=sizex;
		this.sy=sizey;
		// Now lets put it in the corner
		this.x=this.x+this.sx-this.size; 
		this.y=this.y+this.sy-this.size;
		this.sx=30;
		this.sy=30;
		this.setBounds(this.x, this.y, this.sx, this.sy);
	}
	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		//this.moveToTopPriority();
		this.gm.setToHighestPriority(this.mainCompSmart);
		this.mouseXbeforeTrans=e.getX();
		this.mouseYbeforeTrans=e.getY();
		this.IsMoving=true;
		//Error.log("Mouse pressed, Data: "+this.mouseXbeforeTrans+", "+this.mouseYbeforeTrans);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
		this.IsMoving=false;
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if(this.IsMoving){
			int Distancex=this.mouseXbeforeTrans-arg0.getX();
			int Distancey=this.mouseYbeforeTrans-arg0.getY();
			if((this.mainCompSmart.getSizeX()-Distancex)>this.MinX&&(this.mainCompSmart.getSizeY()-Distancey)>this.MinY){
				this.x=this.x-Distancex;
			this.y=this.y-Distancey;
			this.setBounds(this.x, this.y, this.sx, this.sy);
			this.mainCompMovable.TranslateSize(Distancex, Distancey);
			}
			
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	public void setMinX(int x){
		this.MinX=x;
	}
	public void setMinY(int y){
		this.MinY=y;
	}

	@Override
	public void PrintComp(Graphics g) {
		Graphics2D g2= (Graphics2D) g;
		g2.setColor(new Color(51,45,47));
		//g2.setColor(Color.BLACK);
		//g.fillRect(this.x, this.y, this.sizex, this.sizey);
		//g2.drawImage(ImageIO.read(new URL("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAMDQ8MDBIQDAwMDQwMDAwMEBANDAwMFBEWFhQRFBQYHSggGBoxJxQUITEhJSk3Li4uFx8zODMsNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAKAAoAMBIgACEQEDEQH/xAAZAAEBAQEBAQAAAAAAAAAAAAAEAwUCAQD/xAAmEAACAQIGAwEBAQEBAAAAAAAAAQIDYQQRUXGh8CGBkTGx4dFB/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AByV+CUkdyZNgeI7icpMpGL6wO4+i0PROMX1loRfWBWHoTT9EIRfWJpx7mAml6HUfQOlHuY2jHuYGhh0auFX5/wzMOu5mphV+f8AQPsSu5GViFfg1sV3yZWI75Azq0e5Aqse5Da77mBqsAtRdyDTj3IRUYabAjON+CMlfgpNkZMDmWZJlZd8EpIDxMpF3JqJSMWBWLuWg7kYxZaEWBem7iabuGhFiacWAuluOo78AaUWOoqQGjh89eGauFz8eeGZWHUteDUwql488AdYrPXhmViM+pmpiVLXgysRnrwBn1s+pgqufUxtfPXgDVz14AJUz6g9TMRUz14DVN+AITzIyzLT34Iy74AhJ2OGdyiibSA6iisURiViBWKLQRCKLQQCIIRTDQQinEBlIbR75A0ojaMQNLDu3JqYV/nhfTKw8DUwsPwD3FOy+mViJLRfTUxVNGViIIAFeVl9A1ZLTkbWggNWCALUktOQ1SVuRFSKDTigIzduSMnYrOKIyigOZQVibirFZSJOQH0SsfXJJMpGQFo+uSsPXJCMi0JAIh65E01tyFhK4mnO4DKS25G0VtyBpTuNo1LgaWHjtyamFh+fnJlYepdmrhav55YHuKhtyZWIhsauKq3ZlYipcDOrw2A1YbD69S4GrMAdSGwacNhVSYacwDzgrEZQVi85EZSAjLYm0WkSkB5FFYoksjuKWgF4rYtCIeKWhaCWgCYIRTQWCWgmmlowGUkNooDSS0Y2ilo++gNLDo1MKvzwZWHS0ffRq4XLx4ffQHuJVjKxCszVxOWj76MrEZaPvoDOrqzA1VZj62V++gNX2AOorMNNWYqp7DzANNbkZbMRMjICUibO5N2Jv0B6ikWSiVjkBWLLQZGJaGQCIMTTYWGWomnuAyl38G0e/gGluNou4Gnh+/hq4X/zx/DIw7v/AA1cLL88rgDrFK38MrELvg1MVK64MnESugAV13wBqjK8nqgNWTsAaogtRCKknYNUk7ARmRkUlJ2IybsBOSdick7FZSepJu4HiWxSK2OEUiloBSK2LQWxKKWhWCsBeC2E01sHhFaCacbAKpLYdRWwKlEbRiBo4eLtwamFi/H5wZeHiamFi/H4B7iou3BlYiLtwauKTt8MrEZ6r4Bn107cAaqdh1bPVfANXPVfACVE7BqidhNRvVfA029V8AhNOxGSdi0m9V8Iyk9QJyW3wlJdyKSluSlID5d8FI98E0zuLAtHvgtDvghGVi0JWATT74E0++AkJWE05WAbS34HUc9eDPpTsNozsBqYfPVfP9NXCr88r5/pj4edjUwtT88MCuKV18/0ysQtvn+mlialmZWInZgBrrb4Cqrb4KrTswNWdmAeotvgaotvhepOzDTluBKS2+EJLb4VnLcjKW4E5d8E2VkTYH0Skc9GcRZSLApHPRloOzJRZaDAtB2Ymm7MPBiabYCqTs/g2i7P4DpNjaLYGhh3Z/DUwr/PD+GZh8zUwufgD7FSs/hlYh24NbFZmTiMwM6u+5Aar7kPr5gaqAHUfcg033ITUQeogDz74Iy74LSRGSAjJE5IvLvkk++QOEykWcpvUpFvUDuL2KwOIt6/wtBvX+AUgJpojCT1/gqnJ6/wC9JdyGUY3D0pPX+DaMnqAzDwvwamFh+eQOHk9TUwsn48gTxNO5lYiFzaxUnqZWId+QMqtC4KrC5pV33MDVfcwAVIXDTjcZUfcw1TvkAs4kZREz75Iy75A//Z")), 10, 30, 30);
		g2.fillRoundRect(this.x, this.y, this.sx, this.sy, 5,5);
		g2.setColor(Color.gray);
		
	}
	@Override
	public void onCommand(Command c) {
	}

}
