package me.jy89hui.Objects_2DRenderingSystem;

import java.awt.Color;  
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Command;
import me.jy89hui.Interfaces_2DRenderingSystem.MovableComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;
 
public class BallFrame extends SmartComponent implements MovableComponent,Runnable{
	private int x;
	private int y;
	private int sx;
	private int sy;
	private int ballX;
	private int ballY;
	private int ballvelX=(int) ((Math.random()-Math.random())*15);
	private int ballvelY=(int) ((Math.random()-Math.random())*15);
	private Color ballColor = new Color(12,242,176);
	private int ballSize=10;
	private DragBar dragBar;
	private String name;
	private ComponentScaler compScaler;
	private JFrame frame;
	private GraphicsMain gMain;
	public BallFrame(int x,int y, int sx, int sy, GraphicsMain gm){
		JPanel p = gm.getPanel();
		this.x=x;
		this.y=y;
		this.sx=sx;
		this.sy=sy;
		this.gMain=gm;
		this.frame=gm.getFrame();
		this.setBounds(x, y, sx, sy);
		this.ballX=(sx/2)+x;
		this.ballY=(sy/2)+x;
		dragBar=new DragBar(x,y,sx,sy,this,this.gMain);
		compScaler=new ComponentScaler(x,y,sx,sy,this,this.gMain);
		p.add(dragBar);
		p.add(compScaler);
		this.name="BallFrame";
		(new Thread(this)).start();

	}

	public void setName(String n){
		this.name=n;
	}
	public void TranslateLocation(int dx, int dy){
		this.x=this.x-dx;
		this.y=this.y-dy;
		/*this=this.x-dx;
		this.y=this.y-dy;*/
		this.dragBar.Update(this.x, this.y, this.sx, this.sy);
		this.compScaler.Update(this.x, this.y, this.sx, this.sy);
	}
	public void TranslateSize(int sx, int sy){
		this.sx=this.sx-sx;
		this.sy=this.sy-sy;
		this.dragBar.Update(this.x, this.y, this.sx, this.sy);
	}

	@Override
	public void PrintComp(Graphics g) {
		
		g.setColor(new Color(210,207,227)	);
		g.fillRoundRect(this.x, this.y, this.sx, this.sy,5,5);
		g.setColor(this.ballColor);
		g.fillOval(this.ballX, this.ballY, this.ballSize, this.ballSize);
		this.dragBar.PrintComp(g);
		this.compScaler.PrintComp(g);
		//Error.log("Running, Data: "+this.ballX+", "+this.ballY+", "+this.ballvelX+", "+this.ballvelY);
		
	}
	private void UpdateBallVel(){
		if(this.ballX>=this.sx+this.x-this.ballSize){
			this.ballX=this.sx+this.x-this.ballSize;
			this.ballvelX=0-this.ballvelX;
		}
		if(this.ballY>=this.sy+this.y-this.ballSize){
			this.ballY=this.sy+this.y-this.ballSize;
			this.ballvelY=0-this.ballvelY;
		}
		if(this.ballX<=this.x){
			this.ballX=this.x;
			this.ballvelX=0-this.ballvelX;
		}
		if(this.ballY<=this.y){
			this.ballY=this.y;
			this.ballvelY=0-this.ballvelY;
		}
	}
	@Override
	public void run() {
		//Error.log("Run called");
		while(1>0){ // Create a forever loop so it keeps running until it dies.

			this.ballX=this.ballX+this.ballvelX;
			this.ballY=this.ballY+this.ballvelY; // Move the ball
			UpdateBallVel();
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
				logger.FatalError("Cannot move ball. Interupted expetion in class: BallFrame, Method: run()");
				
			}
		}
		
	}
	public String getName(){
		return this.name;
	}
	public void onCommand(Command c){
		if( c.getArgs()[1].equalsIgnoreCase("SetSize")){
			try{
			this.sx=Integer.parseInt(c.getArgs()[2]);
			this.sy=Integer.parseInt(c.getArgs()[3]);
			this.ballX=(this.sx/2)+this.x;
			this.ballY=(this.sy/2)+this.x;
			this.dragBar.Update(this.x, this.y, this.sx, this.sy);
			this.compScaler.Update(this.x, this.y, this.sx, this.sy);
			c.Handled();
			} catch (Exception e){logger.normalError("INVALID COMMAND USE!"); logger.normalError("BallFrame Setsize <SizeXInt> <SizeYInt>");}
		}
		else if( c.getArgs()[1].equalsIgnoreCase("SetLocation")){
			try{
			this.x=Integer.parseInt(c.getArgs()[2]);
			this.y=Integer.parseInt(c.getArgs()[3]);
			this.ballX=(this.sx/2)+this.x;
			this.ballY=(this.sy/2)+this.x;
			this.dragBar.Update(this.x, this.y, this.sx, this.sy);
			this.compScaler.Update(this.x, this.y, this.sx, this.sy);
			c.Handled();
			} catch (Exception e){logger.normalError("INVALID COMMAND USE!"); logger.normalError("BallFrame SetLocation <LocXInt> <LocYInt>");}
		}
		else if( c.getArgs()[1].equalsIgnoreCase("setname")){
			try{
				this.name=c.getArgs()[2];
				c.Handled();
			} catch (Exception e){logger.normalError("INVALID COMMAND USE!"); logger.normalError("BallFrame Setname <NameString>");}
		}
		else if( c.getArgs()[1].equalsIgnoreCase("SetBallSpeed")){
			try{
				this.ballvelX=Integer.parseInt(c.getArgs()[2]);
				this.ballvelY=Integer.parseInt(c.getArgs()[3]);
				c.Handled();
				} catch (Exception e){logger.normalError("INVALID COMMAND USE!"); logger.normalError("BallFrame SetBallSpeed <SpeedXInt> <SpeedYInt>");}
		}
		else if( c.getArgs()[1].equalsIgnoreCase("RandomBallSpeed")){
			try{
				this.ballvelX=(int) ((Math.random()-Math.random())*19);
				this.ballvelY=(int) ((Math.random()-Math.random())*19);
				c.Handled();
				} catch (Exception e){logger.normalError("INVALID COMMAND USE!"); logger.normalError("BallFrame RandomBallSpeed");}
		}
		else if( c.getArgs()[1].equalsIgnoreCase("newBallColor")){
			try{
				this.ballColor= (new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)));
				c.Handled();
				} catch (Exception e){logger.normalError("INVALID COMMAND USE!"); logger.normalError("BallFrame NewBallColor");}
		}
		else if( c.getArgs()[1].equalsIgnoreCase("SetBallSize")){
			try{
				this.ballSize=Integer.parseInt(c.getArgs()[2]);
				this.compScaler.setMinX(this.ballSize);
				this.compScaler.setMinY(this.ballSize);
				c.Handled();
				} catch (Exception e){logger.normalError("INVALID COMMAND USE!"); logger.normalError("BallFrame SetBallSize <SizeInt>");}
		}
		else {
			logger.normalError("There is an error prossing this command. No command with that name has been found in the object: "+this.name);
		}
	}

}
class Ball{
	
}
