package me.jy89hui.Objects_2DRenderingSystem;

import java.awt.Button;  
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Action;
import me.jy89hui.Interfaces_2DRenderingSystem.Command;
import me.jy89hui.Interfaces_2DRenderingSystem.MovableComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartButton;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class Menu extends SmartComponent implements MovableComponent{
	private ArrayList<Button> buttons = new ArrayList<Button>();
	private int x;
	private int y;
	private int sx;
	private int sy;
	private DragBar db;
	private ComponentScaler cs;
	private boolean Movable=true;
	private SmartButton backButton;
	private GraphicsMain gMain;
	private ArrayList<MenuBox> boxes = new ArrayList<MenuBox>();
	private ArrayList<MenuBox> Originalboxes = new ArrayList<MenuBox>();
	
	public Menu(int x,int y,int sx,int sy,boolean movable,GraphicsMain gm, ArrayList<MenuBox> b0xes){
		JPanel jp = gm.getPanel();
		this.boxes = b0xes;
		for(int x1=0; x1< b0xes.size();x1++){
			this.Originalboxes.add(b0xes.get(x1));
		} // This is so Original Boxes does not develop a link to B0xes.
		this.x=x;
		this.y=y;
		this.sy=sy;
		this.sx=sx;
		this.gMain=gm;
		this.Movable=movable;
		this.backButton= new SmartButton(this.x+10, this.y+10, 40,30,gm);
		this.backButton.setBounds(this.x+10, this.y+10, 40, 30);
		this.backButton.setText("Back");
		this.backButton.setColor(new Color(50,88,92));
		this.backButton.enableAction(new Action()
		{
			  @Override
			  public void go()
			  {
				  resetBoxes();
				  logger.log("Back to main menu.");
			  }
		});
		jp.add(this.backButton);
		this.setName("Menu");
		//this.boxes.add(new MenuBox("TestBox", null, false, null));
		//this.boxes.add(new MenuBox("TestBox2", null, false, null));
		if (this.Movable){
			this.db=new DragBar(this.x,this.y,this.sx,this.sy,this,this.gMain);
			this.cs=new ComponentScaler(this.x,this.y,this.sx,this.sy,this,this.gMain);
			jp.add(this.db);
			jp.add(this.cs);
		} 
		int x1=0;
		for(int l=0; l<this.boxes.size(); l++){
			x1++;
			this.boxes.get(l).load((int) (this.x+(this.sx/2)-this.boxes.get(l).getSizeX()/2), this.y+(x1*60)-50, this.sx/2, 50, this.gMain.getPanel());
		}
		this.reLoadBoxes();
		jp.add(this);
	}

	public void resetBoxes(){
		this.clearMenuBoxes();
		for(int x1=0; x1< this.Originalboxes.size();x1++){
			this.boxes.add(this.Originalboxes.get(x1));
		}
		this.reLoadBoxes();
	}
	public void reLoadBoxes(){
		int x1=0;
		for(int l=0; l<this.boxes.size(); l++){
			x1++;
			this.boxes.get(l).load((int) (this.x+(this.sx/2)-this.boxes.get(l).getSizeX()/2), this.y+(x1*60)-50, this.sx/2, 50, this.gMain.getPanel());
		}
	}
	public ArrayList<MenuBox> getBoxes(){
		return this.boxes;
	}
	public void addMenuBox(MenuBox m){
		this.boxes.add(m);
		this.reLoadBoxes();
		int x1=0;
		for(int l=0; l<this.boxes.size(); l++){
			x1++;
			this.boxes.get(l).update((int) (this.x+(this.sx/2)-this.boxes.get(l).getSizeX()/2), this.y+(x1*60)-50, this.sx/2, 50);
		}
	}
	public void clearMenuBoxes(){
		for( int x=0; x<this.boxes.size();x++){
	this.gMain.getPanel().remove(this.boxes.get(x));
		}
		this.boxes.clear();
	}
	public void Destroy(){
		for(int x=0; x<this.gMain.RealComponents.size();x++){
			if (this.gMain.RealComponents.get(x).equals(this.gMain.menu)){
				this.gMain.RealComponents.remove(x);
			}
		}
		this.clearMenuBoxes();
		this.gMain.getPanel().remove(this.backButton);
		if (this.Movable){
		this.gMain.getPanel().remove(this.cs);
		this.gMain.getPanel().remove(this.db);
		}
	}
	@Override
	public void onCommand(Command c) {
		if( c.getArgs()[1].equalsIgnoreCase("SetSize")){
			try{
			this.sx=Integer.parseInt(c.getArgs()[2]);
			this.sy=Integer.parseInt(c.getArgs()[3]);
			this.db.Update(this.x, this.y, this.sx, this.sy);
			this.cs.Update(this.x, this.y, this.sx, this.sy);
			c.Handled();
			} catch (Exception e){logger.normalError("INVALID COMMAND USE!"); logger.normalError(this.getName()+" Setsize <SizeXInt> <SizeYInt>");}
		}
		else if( c.getArgs()[1].equalsIgnoreCase("SetLocation")){
			try{
			this.x=Integer.parseInt(c.getArgs()[2]);
			this.y=Integer.parseInt(c.getArgs()[3]);
			this.db.Update(this.x, this.y, this.sx, this.sy);
			this.cs.Update(this.x, this.y, this.sx, this.sy);
			c.Handled();
			} catch (Exception e){logger.normalError("INVALID COMMAND USE!"); logger.normalError(this.getName()+" SetLocation <LocXInt> <LocYInt>");}
		}
		else if( c.getArgs()[1].equalsIgnoreCase("setname")){
			try{
				this.setName(c.getArgs()[2]);
			} catch (Exception e){logger.normalError("INVALID COMMAND USE!"); logger.normalError(this.getName()+" Setname <NameString>");}
		}
		else if( c.getArgs()[1].equalsIgnoreCase("SomeMenuCommand")){
			try{
				logger.log("PROGRESS");
				c.Handled();
				} catch (Exception e){logger.normalError("INVALID COMMAND USE!"); logger.normalError(this.getName()+" Something... ");}
		}
		else {
			logger.normalError("There is an error prossing this command. No command with that name has been found in the object: "+this.getName());
		}
	}

	@Override
	public void PrintComp(Graphics g) {
		g.setColor(new Color(9,63,87));
		g.fillRoundRect(this.x, this.y, this.sx, this.sy, 10, 10);
		if(this.Movable){
			this.db.PrintComp(g);
			this.cs.PrintComp(g);
		}
		for(int l=0; l<this.boxes.size(); l++){
			this.boxes.get(l).PrintComp(g);
		}
		if (!this.Originalboxes.equals(this.boxes)){
		this.backButton.PrintComp(g);
		}
	}

	@Override
	public void TranslateLocation(int dx, int dy) {
		this.x=this.x-dx;
		this.y=this.y-dy;
		this.db.Update(this.x,this.y,this.sx,this.sy);
		this.cs.Update(this.x,this.y,this.sx,this.sy);
		int x1=0;
		for(int l=0; l<this.boxes.size(); l++){
			x1++;
			this.boxes.get(l).update((int) (this.x+(this.sx/2)-this.boxes.get(l).getSizeX()/2), this.y+(x1*60)-50, this.sx/2, 50);
		}
		this.backButton.setBounds(this.x+10, this.y+10, 40, 30);
		this.backButton.Update(this.x+10, this.y+10, 40, 30);
	}

	@Override
	public void TranslateSize(int sx, int sy) {
		this.sx=this.sx-sx;
		this.sy=this.sy-sy;	
		this.db.Update(this.x,this.y,this.sx,this.sy);
		int x1=0;
		for(int l=0; l<this.boxes.size(); l++){
			x1++;
			this.boxes.get(l).update((int) (this.x+(this.sx/2)-(this.boxes.get(l).getSizeX()/2)), this.y+(x1*60)-50, this.sx/2, 50);
		}
		this.backButton.setBounds(this.x+10, this.y+10, 40, 30); 
		this.backButton.Update(this.x+10, this.y+10, 40, 30);
	}

}
