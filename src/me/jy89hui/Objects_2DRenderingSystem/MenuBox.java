package me.jy89hui.Objects_2DRenderingSystem;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList; 

import javax.swing.JPanel;

import me.jy89hui.Interfaces_2DRenderingSystem.Program;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartLog;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;


public class MenuBox extends SmartComponent implements MouseListener{
	private int x,y,sx,sy;
	private SmartLog text;
	public MenuBox(String n){
		this.text=new SmartLog(n);
		addMouseListener( this);
	}
	public MenuBox(SmartLog log){
		this.text=log;
		addMouseListener( this);
	}
	public void clicked(){
		logger.FatalError("Menubox clicked method MUST Be overriden. Cannot run.");
	}
	public void load(int x,int y, int sx, int sy, JPanel pane){
		this.x=x;
		this.y=y;
		this.sy=sy;
		this.sx=sx;
		this.setBounds(x, y, sx, sy);
		pane.add(this);
		pane.repaint();
	}
	public void update(int x,int y, int sx, int sy){
		this.x=x;
		this.y=y;
		this.sy=sy;
		this.sx=sx;
		this.setBounds(x, y, sx, sy);
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		
		//this.moveToTopPriority();
		this.clicked();
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
	}
	public void PrintComp(Graphics g){
		g.setColor(new Color(27,58,161));
		g.fillRoundRect(this.x, this.y, this.sx, this.sy, 5, 5);
		g.setColor(new Color(0,0,0)); 
		g.setFont(this.text.getFonts().get(0));
		g.drawString(this.text.getMessage(), this.x+(this.sx/2)-(this.getName().length()*4), this.y+(this.sy/2));
	}
}
