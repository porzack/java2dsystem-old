package me.jy89hui.Objects_2DRenderingSystem;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Action;
import me.jy89hui.Interfaces_2DRenderingSystem.Command;
import me.jy89hui.Interfaces_2DRenderingSystem.MovableComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.Program;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartButton;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartTextArea;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartTextArea.typeOfTextArea;
import me.jy89hui.ManagingSystems_2DRenderingSystem.CommandManager;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class ConsoleLogger extends SmartComponent implements MovableComponent,
		KeyListener {
	private int locx = 0;
	private int locy = 0;
	private int sizex = 200;
	private int sizey = 200;
	private int upArrowCount = 0;
	private String realText = "";
	private SmartTextArea text;
	private SmartTextArea terminal;
	private SmartButton GoButton;
	private DragBar dragBar;
	private ArrayList<Command> lastCommands = new ArrayList<Command>();
	private ComponentScaler compScaler;
	private CommandManager commandM;
	private GraphicsMain gMain;
	private Program Requester = null;
	private int RequestTimes = 0; // How many times the program has requested to
									// get the command

	// private Panel p = new Panel();

	public ConsoleLogger(int x, int y, int sx, int sy, GraphicsMain gm,
			CommandManager cm) {
		JPanel p = gm.getPanel();
		this.setLocX(x);
		this.setLocY(y);
		this.setSizeX(sx);
		this.setSizeY(sy);
		this.gMain=gm;
		this.text = new SmartTextArea(0, 0, 0, 0,this.gMain);
		this.terminal = new SmartTextArea(0, 0, 0, 0,this.gMain);
		this.GoButton = new SmartButton(0, 0, 0, 0,this.gMain);
		this.text.setLineScrolling(true);
		p.add(terminal);
		p.add(GoButton);
		p.add(text);
		this.terminal.textAreaType = typeOfTextArea.TextInput;
		this.text.textAreaType = typeOfTextArea.Logger;
		this.gMain = gm;
		this.terminal.addKeyListener(this);
		this.commandM = cm;
		dragBar = new DragBar(x, y, sx, sy, this, this.gMain);
		p.add(dragBar);
		setCompScaler(new ComponentScaler(x, y, sx, sy, this, this.gMain));
		p.add(compScaler);
		this.gMain.getPanel().add(compScaler);
		this.gMain.getPanel().add(dragBar);
		this.gMain.getFrame().add(compScaler);
		this.gMain.getFrame().add(dragBar);
		this.compScaler.setMinX(200);
		this.compScaler.setMinY(150);
		this.setBounds(x, y, sx, sy);
		this.setBackground(Color.BLACK);
		p.add(this);
		this.setVisible(true);
		this.setName("Console");
		gm.add(this);
		p.add(this);
		// p.add(this);

		terminal.setText("");
		UpdateText();
		Load();
		this.UpdateComponents();
	}

	public void resetBounds() {
		this.setBounds(this.locx, this.locy, this.sizex, this.sizey);
	}

	public void RequestNextCommand(Program p) {
		this.Requester = p;
		this.RequestTimes = 1;
		logger.silentlog("Program " + p.getName()
				+ " has requested the next command");
	}

	public void RequestNextCommands(Program p, int commandTimes) {
		this.Requester = p;
		this.RequestTimes = commandTimes;
		logger.silentlog("Program " + p.getName() + " has requested the next "
				+ commandTimes + " commands");
	}

	public void Load() {
		this.setBounds(locx, locy, sizex, sizey);
		this.setBackground(Color.gray);

		LoadJFieldAndTerminal();
		this.setVisible(true);

	}

	public void UpdateText() {
		this.text.setTextSL(this.gMain.LogRecord);
	}

	public void UpdateComponents() {
		this.terminal.Update(
				(int) ((((float) this.sizex / 100) * 5) + this.locx),
				(int) ((((float) this.sizey / 100) * 87) + this.locy),
				(int) (((float) this.sizex / 100) * 80),
				(int) (((float) this.sizey / 100) * 10));
		this.text.Update((int) ((((float) this.sizex / 100) * 5) + this.locx),
				(int) ((((float) this.sizey / 100) * 4) + this.locy),
				(int) (((float) this.sizex / 100) * 90),
				(int) (((float) this.sizey / 100) * 80));
		this.GoButton.Update(
				(int) ((((float) this.sizex / 100) * 86) + this.locx),
				(int) ((((float) this.sizey / 100) * 87) + this.locy),
				(int) (((float) this.sizex / 100) * 9),
				(int) (((float) this.sizey / 100) * 10));
		this.compScaler.Update(this.locx, this.locy, this.sizex, this.sizey);
		this.dragBar.Update(this.locx, this.locy, this.sizex, this.sizey);
	}

	public void LoadJFieldAndTerminal() {
		UpdateComponents();
		this.text.setEditable(false);
		this.text.setLineWrap(true);
		this.terminal.setEditable(true);
		this.terminal.setVisible(true);
		this.GoButton.setText("Enter");
		this.GoButton.enableAction(new Action() {
			@Override
			public void go() {
				onCommandEnter();
			}
		});
	}

	public void onCommandEnter() { // When a command has been entered
		this.upArrowCount = 0;
		if (getTerminal().getText().equals("END") && this.RequestTimes > 0) {
			this.RequestTimes = 0;
			this.Requester = null;
			getTerminal().setText("");
			logger.log("-----------------------------------------", new Color(
					0, 150, 158));
			logger.log("Request Terminated. Back to Main Console.", new Color(
					0, 150, 158));
		} else {
			if (this.gMain.Console.Requester == null) {
				ManageCommand(getTerminal().getText());
				getTerminal().setText(""); // <- Clear the text
			} else { // Someone did request something
				String[] SplitC = getTerminal().getText().split(" ");
				String cmd = SplitC[0];
				this.Requester.giveData(getTerminal().getText());
				this.lastCommands.add((new Command(cmd, SplitC, getTerminal()
						.getText())));
				getTerminal().setText(""); // <- Clear the text
				this.RequestTimes--;
				if (this.RequestTimes <= 0) {
					this.Requester = null;
				}
			}
		}
	}

	private void ManageCommand(String c) {
		String[] SplitC = c.split(" ");
		String cmd = SplitC[0];
		this.lastCommands
				.add((new Command(cmd, SplitC, getTerminal().getText())));
		this.commandM.Manage(new Command(cmd, SplitC, c), this.gMain);

	}

	@Override
	public void PrintComp(Graphics g) {
		g.setColor(new Color(150, 134, 139));
		g.fillRoundRect(this.locx, this.locy, this.sizex, this.sizey, 10, 10);
		this.compScaler.PrintComp(g); // call the other 2 component print
										// methods
		this.dragBar.PrintComp(g); // After so that they have priority (above
									// main comp)
		this.terminal.PrintComp(g);
		this.text.PrintComp(g);
		this.GoButton.PrintComp(g);
	}

	@Override
	public void TranslateLocation(int dx, int dy) {
		this.locx = this.locx - dx;
		this.locy = this.locy - dy;
		this.dragBar.Update(this.locx, this.locy, this.sizex, this.sizey);
		this.compScaler.Update(this.locx, this.locy, this.sizex, this.sizey);
		UpdateComponents();
	}

	@Override
	public void TranslateSize(int sx, int sy) {
		this.sizex = this.sizex - sx;
		this.sizey = this.sizey - sy;
		resetBounds();
		this.dragBar.Update(this.locx, this.locy, this.sizex, this.sizey);
		UpdateComponents();
	}

	@Override
	public void onCommand(Command c) {
		if (c.getArgs()[1].equalsIgnoreCase("SetSize")) {
			try {
				this.sizex = Integer.parseInt(c.getArgs()[2]);
				this.sizey = Integer.parseInt(c.getArgs()[3]);
				UpdateComponents();
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName()
						+ " Setsize <SizeXInt> <SizeYInt>");
			}
		} else if (c.getArgs()[1].equalsIgnoreCase("SetLocation")) {
			try {
				this.locx = Integer.parseInt(c.getArgs()[2]);
				this.locy = Integer.parseInt(c.getArgs()[3]);
				UpdateComponents();
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName()
						+ " SetLocation <LocXInt> <LocYInt>");
			}
		} else if (c.getArgs()[1].equalsIgnoreCase("setname")) {
			try {
				this.setName(c.getArgs()[2]);
				c.Handled();
			} catch (Exception e) {
				logger.normalError("INVALID COMMAND USE!");
				logger.normalError(this.getName() + " Setname <NameString>");
			}
		} else {
			logger.normalError("There is an error prossing this command. No command with that name has been found in the object: "
					+ this.getName());
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			onCommandEnter();
			// this.terminal.setEnabled(true);
		} else if (e.getKeyCode() == KeyEvent.VK_UP) {
			this.upArrowCount++;
			if (this.lastCommands.size() >= this.upArrowCount) {
				this.terminal.setText(this.lastCommands.get(
						this.lastCommands.size() - this.upArrowCount)
						.getFullCommand());
				//TODO: Fix this setCaretPosition junk.
				try {
					this.terminal.setCaretPosition(this.lastCommands
							.get(this.upArrowCount).getFullCommand().length());
				} catch (IllegalArgumentException e2) {
				}
				 catch (IndexOutOfBoundsException e2) {
				}// Swings setcaretposition does not work as I assume it should
					// but this forces the caret to the end OR NOT
			} else {
				this.upArrowCount--;
			}
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			this.upArrowCount--;
			if (this.upArrowCount > 0) {
				this.terminal.setText(this.lastCommands.get(
						this.lastCommands.size() - this.upArrowCount)
						.getFullCommand());
				this.terminal.setCaretPosition(this.lastCommands
						.get(this.upArrowCount).getFullCommand().length());
				try {
				} catch (IllegalArgumentException e2) {
				} // Swings setcaretposition does not work as I assume it should
					// but this forces the caret to the end
			} else {

				this.upArrowCount++;
			}
		}

	}

	public DragBar getDragBar() {
		return this.dragBar;
	}

	public void setDragBar(DragBar dragBar) {
		this.dragBar = dragBar;
	}

	public ComponentScaler getCompScaler() {
		return this.compScaler;
	}

	public void setCompScaler(ComponentScaler compScaler) {
		this.compScaler = compScaler;
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public int getLocX() {
		return this.locx;
	}

	public void setLocX(int locx) {
		this.locx = locx;
	}

	@Override
	public int getLocY() {
		return this.locy;
	}

	public void setLocY(int locy) {
		this.locy = locy;
	}

	@Override
	public int getSizeX() {
		return this.sizex;
	}

	public void setSizeX(int sizex) {
		this.sizex = sizex;
	}

	@Override
	public int getSizeY() {
		return this.sizey;
	}

	public void setSizeY(int sizey) {
		this.sizey = sizey;
	}

	public JTextArea getText() {
		return this.text;
	}

	public void setText(SmartTextArea text) {
		this.text = text;
	}

	public SmartTextArea getTerminal() {
		return this.terminal;
	}

	public void setTerinal(SmartTextArea Terminal) {
		this.terminal = Terminal;
	}

	public String getRealText() {
		return this.realText;
	}

	public void setRealText(String realText) {
		this.realText = realText;
	}
}
