
package me.jy89hui.AMain;

import java.awt.Color; 
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.Timer;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Command;
import me.jy89hui.Interfaces_2DRenderingSystem.Location;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.ManagingSystems_2DRenderingSystem.CommandManager;
import me.jy89hui.ManagingSystems_2DRenderingSystem.PaintManager;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;
import me.jy89hui.Objects_2DRenderingSystem.BallFrame;
import me.jy89hui.Objects_2DRenderingSystem.ConsoleLogger;
import me.jy89hui.Objects_2DRenderingSystem.Menu;
import me.jy89hui.Objects_2DRenderingSystem.MenuBox;



/*
 * |-------------------|
 * |  12.5             |
 * |            72     |
 * |                   |
 * |                   |
 * |      ENTRANCE     |
 * |         TO        |
 * |       MAGIC       |
 * |      NUMBER       |
 * |       HELL!       |             <------- ENTER HERE 
 * |                   |
 * |   4.26            |
 * |                   |
 * |       3           |
 * |              22   |
 * |                   |
 * |     12.111        |
 * |                   |
 * |                   |
 * |-------------------|
 * 
 * 
 */
public class Main2D extends GraphicsMain {
	public static Main2D main = (new Main2D());
	public static JPanel Panel = new PaintManager();
	public static CommandManager comandmanager = new ComandControl();
	public static PaintManager paintManager;



	public static void main(String[] args){
		//logger.FatalError("PLEASE DO NOT USE THE MAIN2D CLASS FROM JAVA2DRENDERINGSYSTEM.");
		main.setEqual();
		main.getFrame().setName("ServerManager");
		logger.log("Running Version: "+main.version, new Color(20,75,50) , new Font("Times New Roman", Font.PLAIN, 20));
		Panel.setLayout(null);
		paintManager = new PaintManager();
		paintManager.load(main);
		LoadFrame(0, 0, 2000, 1175, Panel, main);

		main.LoadAcceptedComponents();
		log("Program Started.");

		getFrame().repaint();
		
	}
	public void setEqual(){
		this.main=this;
	}



	public void LoadAcceptedComponents() {
		main.Console = new ConsoleLogger(25, 50, 600, 300, this, comandmanager); 
		main.RealComponents.add(new BallFrame(500, 500, 500,500, this));
		main.RealComponents.add(this.Console);
		// this.RealComponents.add(new BallFrame(650,50,500,500,this));
		ArrayList<MenuBox> boxes = new ArrayList<MenuBox>();
		boxes.add(new MenuBox("Main"){
			@Override
			public void clicked(){
				menu.clearMenuBoxes();
				menu.addMenuBox(new MenuBox("You are an idiot."){
					@Override
					public void clicked(){
						logger.log("You are even more of an idiot.", new Color(255,50,50));
					}
				});
			}
		});
		boxes.add(new MenuBox("Ballframe"){
			@Override
			public void clicked(){
				menu.clearMenuBoxes();
				menu.addMenuBox(new MenuBox("ResetLocation"){
					@Override
					public void clicked(){
						String[] s =  {"BallFrame","setlocation","500", "500",};
						comandmanager.Manage(new Command("BallFrame",s,"BallFrame setlocation 500 500"), main);
					}
				});
				menu.addMenuBox(new MenuBox("ResetSize"){
					@Override
					public void clicked(){
						String[] s =  {"BallFrame","setsize","500", "500",};
						comandmanager.Manage(new Command("BallFrame",s,"BallFrame setsize 500 500"), main);
					}
				});
				menu.addMenuBox(new MenuBox("RandomBallColor"){
					@Override
					public void clicked(){
						String[] s =  {"BallFrame","newBallColor"};
						comandmanager.Manage(new Command("BallFrame",s,"BallFrame newBallColor"), main);
					}
				});
				menu.addMenuBox(new MenuBox("RandomBallSpeed"){
					@Override
					public void clicked(){
						String[] s =  {"BallFrame","RandomBallSpeed"};
						comandmanager.Manage(new Command("BallFrame",s,"BallFrame RandomBallSpeed"), main);
					}
				});
			}
		});
		this.menu= new Menu(100, 500, 500,500, true, this, boxes);
		logger.log(new Location(5,10).toString(), new Color(0,50,0));
		this.RealComponents.add(this.menu);

	}
}
