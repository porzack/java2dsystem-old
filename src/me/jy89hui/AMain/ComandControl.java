package me.jy89hui.AMain;

import java.util.ArrayList;

import me.jy89hui.ExtendableMains_2DRenderingSystem.GraphicsMain;
import me.jy89hui.Interfaces_2DRenderingSystem.Command;
import me.jy89hui.Interfaces_2DRenderingSystem.SmartComponent;
import me.jy89hui.ManagingSystems_2DRenderingSystem.CommandManager;
import me.jy89hui.ManagingSystems_2DRenderingSystem.logger;

public class ComandControl extends CommandManager{
	private static ArrayList<SmartComponent> comps = Main2D.RealComponents;
	private static GraphicsMain mainclass;
	public static void LoadControl(GraphicsMain gm){
		mainclass=gm;
	}
	private void Load(){
		comps=mainclass.RealComponents;
	}
	@Override
	public void Manage(Command c,GraphicsMain gm){
		Load();
		BasicManage(c, gm);
		boolean handled= false; // This is just if the command has been given to the objects
		for(int x=0; x<comps.size();x++){
			if(c.getCmd().equalsIgnoreCase(comps.get(x).getName())){
				comps.get(x).onCommand(c);
			}
		}
		if (!c.isHandled()){
			logger.normalError("There has been a problem handing that command. ");
			logger.normalError("Command: [ "+c.getFullCommand()+" ]");
		}
	}
}
